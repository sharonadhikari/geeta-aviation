<section id="fh5co-work" data-section="work">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Gallery</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Glimpses of Geeta Aviation</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-bottom-padded-sm">
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air 1.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air 1.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 1</h2>
						<span></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air 2.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air 2.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 2</h2>
						<span></span>
						</div>
					</a>
				</div>

				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air_4.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air_4.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 3</h2>
						<span></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air_8.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air_8.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 4</h2>
						<span></span>
						</div>
					</a>
				</div>
				
				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air_6.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air_6.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 1</h2>
						<span></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air_7.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air_7.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 2</h2>
						<span></span>
						</div>
					</a>
				</div>
				
				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air_11.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air_11.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 3</h2>
						<span></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/air_10.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/air_10.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 4</h2>
						<span></span>
						</div>
					</a>
				</div>

				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/5.jpg" class="fh5co-project-item image-popup to-animate">
						<img src="images/5.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Project 2</h2>
						<span></span>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center to-animate">
					<p>* Demo images from <a href="http://plmd.me/" target="_blank">plmd.me</a></p>
				</div>
			</div>
		</div>
	</section>