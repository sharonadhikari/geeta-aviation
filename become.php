<section id="fh5co-intro">
		<div class="container">
			<div class="row row-bottom-padded-lg">
				<div class="fh5co-block to-animate" style="background-image: url(images/pilot.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="fh5co-text">
						<i class="fh5co-intro-icon icon-plane"></i>
						<h2>Become A <br>
</br> Pilot</h2>
						<p>Imagine how it would feel to have your office in the air. Well, Pilots have it. It is the coolest career to have. You actually get paid to travel while 99.99 percentage of the world pays to travel. Yes its true, there are so few pilots in the world that you will stand out from the crowd anywhere you go.</p>
						<p><a href="BecomeAPilot.php"class="btn btn-primary" >Read More</a></p>
					</div>
				</div>
				<div class="fh5co-block to-animate" style="background-image: url(images/host.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="fh5co-text">
						<i class="fh5co-intro-icon icon-plane"></i>
						<h2>Become An <br>
							</br> Air hostess</h2>
						<p>If you have a pleasing personality and love travelling and getting along with people, you can consider a career in aviation as an air hostess. Becoming an air hostess makes you differ from the rest of the pack. You learn to take responsibilities and duties more seriously as your job demands no less from </p> 
						<p><a href="BecomeAnAirHostess.html"class="btn btn-primary">Read More</a></p>
					</div>
				</div>
				<div class="fh5co-block to-animate" style="background-image: url(images/engine.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="fh5co-text">
						<i class="fh5co-intro-icon icon-plane"></i>
						<h2>Become An <br>
							</br> Aeronautical Engineer</h2>
						<p>It is one of the most sought after professions in the planet. IF you have ever wondered how an aircraft flies, what the best materials are to build the wings and the best shape to make it more efficient. If you have ever thought how the aircraft engines worked and how to harness power from it.</p>
						<p><a href="BecomeAnAeronauticalEngineer.html" class="btn btn-primary">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="row watch-video text-center to-animate">
				<span>Watch the video</span>

					<iframe width="560" height="315" src="https://www.youtube.com/embed/uWxW6vzBHp4" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
				</div>


			</div>
		</div>
	</section>