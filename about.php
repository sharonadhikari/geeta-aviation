<section id="fh5co-about" data-section="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">About</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3> 

Geeta Aviation was established in 2012 with the sole aim to guide promising candidates into the field of aviation. We have since then helped to produce hundreds of able and exceptional work force in Nepalese as well as International Aviation Industry. At Geeta Aviation, all the counselling and trainings are provided by experienced and trained aviation experts who are the best in the field. 

<br>
</br>

We help new prospective aviation enthusiastics and students by providing them with the optimum skill required to achieve their goals and walk them through their journey so the they know what to expect during their training. They will be prepared academically and mentally as we provide classes to all students before they depart for the training.

<br
</br>

<h4>
We have more to offer in aviation than any other Aviation Education Provider!
</h4>

.</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
			<div class="row" >
				<div class="col-md-4">

					<div class="fh5co-person text-center ">
						<figure><img src="images/person1.jpg" alt="Image"></figure>
						<h3>ABC</h3>
						<span class="fh5co-position">Web Designer</span>
						<p>hdghdgvhsvghsv hgshfgahf asgfhfg ahgfgfhf hagfhgfhgh haghgfh hagfhagfha hagfhgfhf ahgfhagfh ahgfhagfh afghagfha hagfhgfh ahghfagfh afghagfh hagfhagf hagfhagfh hagfhagfh ahgfhfgh haghagf hgfahgf hagfhagf </p>
						<ul class="social social-circle">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fh5co-person text-center to-animate">
						<figure><img src="images/person2.jpg" alt="Image"></figure>
						<h3>HGD</h3>
						<span class="fh5co-position">Web Developer</span>
						<p>bsfhvvnvszcb affg fgfa afggfh gfhgfhgsf gshfghaf jghhgj sghshg sghshgj shgsjhgj sjgjhsgj sgjsgj sjghsjhgj sgsgj shgshb ghdghjgb ghdbhv hhghd dhfgh hfghfg hgfdhg hghh ghb hgsdh dhghg ahfsfh jahfjh ajhfjaf jashfjh</p>
						<ul class="social social-circle">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fh5co-person text-center to-animate">
						<figure><img src="images/person3.jpg" alt="Image"></figure>
						<h3>ABH</h3>
						<span class="fh5co-position">Web Designer</span>
						<p>hfhsvvsv hagfsgh afshfgh hsgfhg ahfhf hghdg hgh sfghgf agf jshgh hagfhag ahsfghafg hdjg ahgag afjg ahjh afgf jdhgh agfgf sjhfja ajsgg gdj sfueguef gfhshf asfuge ahgfhgb asfyeg afhfvj ahfghfg habsfghg fhsaff  fhshf</p>
						<ul class="social social-circle">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>