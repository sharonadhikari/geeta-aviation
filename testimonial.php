<section id="fh5co-testimonials" data-section="testimonials">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Testimonials</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>shvsbhjhjhxhdghgc hagffaga hgfhgfsgfhgsf gfsghgsfgshgfgsgf fgsfgsf gsfsgfgf sgfsgfsg gfsgfsgf gsfgsgfshgf sgfhsgfsgf gsfsgfsgf hsgfsgfsg hsgfsgfsg sgfsgug gfshgfs g hsgfsfg sgsgfsfg gfsf gs fsgfsgfhsfgsgfhgfsgfgsf sygfsgfsgfsgf shfgaeyy vfgf gvgsfs gsdvgdge gsfgjhu hgfue hgegu geeu hghegf shjf </h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="box-testimony">
						<blockquote class="to-animate-2">
							<p>&ldquo;vsbhjhjhxhdghgc hagffaga hgfhgfsgfhgsf gfsghgsfgshgfgsgf fgsfgsf gsfsgfgf sgfsgfsg gfsgfsgf gsfgsgfshgf sgfhsgfsgf gsfsgfsgf hsgfsgfsg hsgfsgfsg sgfsgug gfshgfs g hsgfsfg sgsgfsfg gfsf gs fsgfsgfhsfgsgfhgfsgfgsf sygfsgfsgfsgf shfgaeyy vfgf gvgsfs gsdvgdge gsfgjhu hgfue hgegu geeu hghegf shjf &rdquo;</p>
						</blockquote>
						<div class="author to-animate">
							<figure><img src="images/person1.jpg" alt="Person"></figure>
							<p>
							hvsbhjhjhxhdghgc  <a href="http://freehtml5.co/" target="_blank">bfhfghf</a> <span class="subtext">Creative Director</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box-testimony">
						<blockquote class="to-animate-2">
							<p>&ldquo;vsbhjhjhxhdghgc hagffaga hgfhgfsgfhgsf gfsghgsfgshgfgsgf fgsfgsf gsfsgfgf sgfsgfsg gfsgfsgf gsfgsgfshgf sgfhsgfsgf gsfsgfsgf hsgfsgfsg hsgfsgfsg sgfsgug gfshgfs g hsgfsfg sgsgfsfg gfsf gs fsgfsgfhsfgsgfhgfsgfgsf sygfsgfsgfsgf shfgaeyy vfgf gvgsfs gsdvgdge gsfgjhu hgfue hgegu geeu hghegf shjf .&rdquo;</p>
						</blockquote>
						<div class="author to-animate">
							<figure><img src="images/person2.jpg" alt="Person"></figure>
							<p>
							hvsbhjhjhxhdghgc  <a href="http://freehtml5.co/" target="_blank">ffggf</a> <span class="subtext">Creative Director</span>
							</p>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="box-testimony">
						<blockquote class="to-animate-2">
							<p>&ldquo;vsbhjhjhxhdghgc hagffaga hgfhgfsgfhgsf gfsghgsfgshgfgsgf fgsfgsf gsfsgfgf sgfsgfsg gfsgfsgf gsfgsgfshgf sgfhsgfsgf gsfsgfsgf hsgfsgfsg hsgfsgfsg sgfsgug gfshgfs g hsgfsfg sgsgfsfg gfsf gs fsgfsgfhsfgsgfhgfsgfgsf sygfsgfsgfsgf shfgaeyy vfgf gvgsfs gsdvgdge gsfgjhu hgfue hgegu geeu hghegf shjf  &rdquo;</p>
						</blockquote>
						<div class="author to-animate">
							<figure><img src="images/person3.jpg" alt="Person"></figure>
							<p>
							hgssf <a href="http://freehtml5.co/" target="_blank">aggvvv</a> <span class="subtext">Creative Director</span>
							</p>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</section>